import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import CloseIcon from '@material-ui/icons/Close';
import './App.css';

export default class Home extends  Component{



    constructor(props) {
        super(props);
        this.state = {value: '', tasks:[]};


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
       // this.navigateAddTask = this.navigateAddTask.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    componentDidMount(){
        this.getTask();
    }

    handleSubmit(event) {

        let request = {
            title: this.state.value
        }
        this.addTask(request);

        event.preventDefault();
    }

    navigateAddTask(obj){
        localStorage.setItem("id", obj.id);
        localStorage.setItem("title", obj.title);
        localStorage.setItem("description", obj.description);
        let ref = this;
        let route = '/AddTasks';
            route += '?authenticated';
        ref.props.history.push(route);
    }

    render() {

        return (

            <div className="App">
                <div className="div_addnew">
                    <div className="elements_container">
                        <h1> TO-DO: </h1>
                        <form onSubmit={this.handleSubmit}>
                            <label>
                                <input type="text" value={this.state.value} onChange={this.handleChange} />
                            </label><br/>


                            <Button variant="contained" color="primary" type="submit">
                                Add new To-Do
                            </Button>
                            {/*<Route path="/AddTask" component={AddTask}  />*/}
                        </form>
                    </div>
                </div>



                <div className="div_todoItems">
                    <List>
                        {this.generate(

                        )}
                    </List>
                </div>

            </div>

        );
    }



     addTask(taskObj) {
        let ref = this;
        fetch('https://practiceapi.devmountain.com/api/tasks', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(taskObj)
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            ref.setState({tasks:data});
            //alert(JSON.stringify(ref.state.tasks));
        });
    }

     getTask() {
        let ref = this;
        fetch('https://practiceapi.devmountain.com/api/tasks', {
            method: 'GET'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            ref.setState({tasks:data});
        });
    }

    completeTask(id) {
        let ref = this;
        fetch('https://practiceapi.devmountain.com/api/tasks/'+id, {
            method: 'PUT'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            ref.setState({tasks:data});
        });
    }

    deleteTask(id) {
        let ref = this;
        fetch('https://practiceapi.devmountain.com/api/tasks/'+id, {
            method: 'DELETE'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
             ref.setState({tasks:data});
        });
    }

     generate() {
        let items = this.state.tasks;
        return items.map(value =>
            <ListItem>
                <ListItemText
                    primary={value.title}
                    onClick={() => this.navigateAddTask(value)}
                > </ListItemText>
                <ListItemSecondaryAction>
                    <IconButton>
                        <Button variant="contained" color="primary" onClick={() => this.completeTask(value.id)}>
                           Complete
                        </Button>
                        <CloseIcon onClick={() => this.deleteTask(value.id)}></CloseIcon>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}



Home.propTypes = {
  history: PropTypes.object.isRequired
};
