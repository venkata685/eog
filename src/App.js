import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Link
} from 'react-router-dom';
import AddTasks from './AddTasks';
import Home from './Home';
import restricted from './Restricted';

const App = () => (
    <Router>
        <div>

            <main>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/AddTasks" component={restricted(AddTasks)} />
                </Switch>
            </main>
        </div>
    </Router>
);

export default App;
