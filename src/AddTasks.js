import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

import {

} from 'react-router-dom';
import './App.css';


export default class AddTasks extends  Component{

    constructor(props) {
        super(props);


        this.state = {value: '',description: '', };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDesc = this.handleChangeDesc.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){
        let title = localStorage.getItem("title");
        let description = localStorage.getItem("description");
        this.setState({value: title});
        this.setState({description: description});
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleChangeDesc(event) {
        this.setState({description: event.target.value});
    }

    handleSubmit(event) {
        let request = {
            title: this.state.value,
            description: this.state.description
        }

        let id = localStorage.getItem('id');

        this.updateTask(request,id);

        event.preventDefault();
    }
    handleCancel = () => {
        this.setState({
            value: "",
            description: ""
        });
    }


    completeTask(id) {
        let ref = this;
        fetch('https://practiceapi.devmountain.com/api/tasks/'+id, {
            method: 'PUT'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            ref.setState({tasks:data});
            ref.goBack();
        });
    }

    updateTask(taskObj,id) {
        let ref = this;
        fetch('https://practiceapi.devmountain.com/api/tasks/'+id, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(taskObj)
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            ref.setState({tasks:data});
            ref.goBack();
        });
    }

    deleteTask(id) {
        let ref = this;
        fetch('https://practiceapi.devmountain.com/api/tasks/'+id, {
            method: 'DELETE'
        }).then(function(response) {
            return response.json();
        }).then(function(data) {
            ref.goBack();
            ref.setState({tasks:data});
        });
    }

    goBack(){
        this.props.history.goBack();
    }



    render() {
        let id = localStorage.getItem('id');
        return (

            <div className="App">
                <div className="div_addnew">
                    <div className="elements_container">
                        <p onClick={this.props.history.goBack}>  Back to Tasks </p>
                        <form onSubmit={this.handleSubmit} id="create-course-form">
                            <label> Task </label><br/>
                            <div className="TaskAndButton">

                            <label className="task">
                                <input type="text" value={this.state.value} onChange={this.handleChange} />
                            </label><br/>
                                <Button  className="complete_button" variant="contained" color="primary" onClick={() => this.completeTask(id)}>
                                    complete
                                </Button>
                            </div>

                            <label> Description </label><br/>
                            <label>
                                <input type="text" value={this.state.description} onChange={this.handleChangeDesc} />
                            </label><br/>
                            <Button variant="contained" color="primary" type="submit">
                                Save
                            </Button>
                            <Button className="cancel_button" variant="contained" color="primary" onClick={this.handleCancel}>
                                Cancel
                            </Button>
                            <Button className="delete_button" variant="contained" color="primary" onClick={() => this.deleteTask(id)}>
                                Delete
                            </Button>
                            {/*<Route path="/AddTask" component={AddTask}  />*/}
                        </form>
                    </div>
                </div>

            </div>

        );
    }
}

function generate(element) {
    return [0, 1, 2].map(value =>
        React.cloneElement(element, {
            key: value,
        }),
    );
}
